'use strict';
// prettier-ignore
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const resetBtn = document.querySelector('#reset');
const form = document.querySelector('.form');
const cadenceRow = document.querySelector('#cadence');
const elevationRow = document.querySelector('#elevation');
const containerWorkouts = document.querySelector('.workouts');
const inputType = document.querySelector('.form__input--type');
const inputDistance = document.querySelector('.form__input--distance');
const inputDuration = document.querySelector('.form__input--duration');
const inputCadence = document.querySelector('.form__input--cadence');
const inputElevation = document.querySelector('.form__input--elevation');

// let coords = [];
// navigator.geolocation.getCurrentPosition(
//   position => {
//     coords = [position.coords.latitude, position.coords.longitude];
//     function onMapClick(e) {
//       L.marker(e.latlng)
//         .addTo(map)
//         .bindPopup(
//           L.popup({
//             autoClose: false,
//             closeOnClick: false,
//             className: 'running-popup',
//           }).setContent(e.latlng.toString())
//         )
//         .openPopup();
//     }
//     const map = L.map('map').setView(coords, 18);
//     L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
//       maxZoom: 20,
//       subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
//       attribution:
//         '&copy; <a href="https://mapsplatform.google.com/">Google Maps</a> contributors',
//     }).addTo(map);

//     map.on('click', onMapClick);
//   },
//   () => {
//     alert('Unable to fetch your Location. Please allow loccation!');
//   }
// );

/*Can you build this application on your own?*/
/*Definetly I can.*/

class Workout {
  _date;
  _id;
  constructor(distance, duration, coordinates, date) {
    this.distance = distance;
    this.duration = duration;
    this.coords = coordinates;
    this._date = date == undefined ? Date.now() : date;
    this._id = this._date.toString(32);
  }

  get date() {
    return `${new Intl.DateTimeFormat(navigator.language, {
      dateStyle: 'medium',
      timeStyle: 'short',
    }).format(this._date)}`;
  }

  get id() {
    return this._id;
  }

  click(map) {
    //Bring the marker on the center
    map.setView(this.coords);
  }
}

class Running extends Workout {
  constructor(distance, duration, coordinates, date, cadence) {
    super(distance, duration, coordinates, date);
    this.cadence = cadence;
    this.pace = this._calcPace();
    this._setDiscription();
    this.type = 'Running';
  }

  _setDiscription() {
    //Method to set description on the workout objects
    this.description = `Running on ${this.date}`;
  }

  _calcPace() {
    return this.duration / this.distance;
  }
}

class Cycling extends Workout {
  constructor(distance, duration, coordinates, date, elevation) {
    super(distance, duration, coordinates, date);
    this.elevationGain = elevation;
    this.speed = this._calcSpeed();
    this._setDiscription();
    this.type = 'Cycling';
  }

  _setDiscription() {
    //Method to set description on the workout objects
    this.description = `Cycling on ${this.date}`;
  }

  _calcSpeed() {
    return (this.distance / this.duration) * 60;
  }
}

class App {
  #map = null;
  #lastCoords;
  #workouts = [];
  constructor() {
    this.mapZoomLevel = 16;

    //This is the startup place from where the code will run
    this._getPosition();

    //Submitting should create workout(eventListener)
    form.addEventListener('submit', this._newWorkout.bind(this));

    //Toggle cadence/elevation based on input
    inputType.addEventListener('change', ele => {
      elevationRow.classList.toggle('form__row--hidden');
      cadenceRow.classList.toggle('form__row--hidden');
    });

    //Reset App
    resetBtn.addEventListener('click', e => {
      e.preventDefault();
      this.resetLocalStorage();
    });
  }

  _getLocalStorage() {
    const workouts = JSON.parse(localStorage.getItem('myWorkouts'));
    if (workouts) {
      this.#workouts = workouts;
    } else {
      return;
    }

    this.#workouts = workouts.map(workout => {
      workout = Object.setPrototypeOf(
        workout,
        workout.type == 'Running' ? Running.prototype : Cycling.prototype
      );
      return workout;
    });

    this.#workouts.forEach(ele => {
      this._renderUI(ele);
    });

    console.log(this.#workouts);
  }

  _setLocalStorage() {
    console.log(this.#workouts);
    const markers = this.#workouts.map(work => {
      return work.marker;
    });
    this.#workouts.forEach(workout => {
      workout.marker = null;
    });
    const data = JSON.stringify(this.#workouts);
    localStorage.setItem('myWorkouts', data);
    this.#workouts.forEach((w, i) => {
      w.marker = markers[i];
    });
  }

  _getPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        this._loadMap.bind(this),
        function () {
          alert('Unable to get locations. Please grant permission');
        }
      );
    }
  }

  _loadMap(position) {
    const coords = [position.coords.latitude, position.coords.longitude];
    this.#map = L.map('map');

    L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
      maxZoom: 20,
      subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
      attribution:
        '&copy; <a href="https://mapsplatform.google.com/">Google Maps</a> contributors',
    }).addTo(this.#map);

    this.#map.on('click', this._showForm.bind(this));
    this._getLocalStorage();

    this.#map.setView(coords, this.mapZoomLevel);
  }

  _showForm(mapE) {
    this.#lastCoords = [mapE.latlng.lat, mapE.latlng.lng];
    form.classList.remove('hidden');
  }

  _editForm(workout, tag) {
    inputType.disabled = true;
    form.removeEventListener('submit', this._newWorkout.bind(this));
    function editFn(e) {
      console.log('edit form');
      workout.distance = inputDistance.value;
      workout.duration = inputDuration.value;
      if (workout instanceof Running) {
        workout.cadence = inputCadence.value;
      } else {
        workout.elevation = inputElevation.value;
      }
    }
    form.classList.remove('hidden');
    form.addEventListener('submit', editFn);
  }

  _newWorkout(event) {
    console.log('new workout');
    event.preventDefault();
    console.log(this.#lastCoords);
    const type = inputType.value;
    let workout;

    function verifyInputs(...inputs) {
      let returnVal = true;
      inputs.forEach(element => {
        if (!Number.isFinite(parseInt(element))) {
          returnVal = false;
        }
      });

      return returnVal;
    }

    switch (type) {
      case 'running':
        {
          const distance = inputDistance.value;
          const duration = inputDuration.value;
          const coords = this.#lastCoords;
          const cadence = inputCadence.value;

          if (!verifyInputs(distance, duration, cadence)) {
            alert('Please Enter correct values!!!');
            return;
          }

          workout = new Running(
            distance,
            duration,
            coords,
            Date.now(),
            cadence
          );
        }
        break;
      case 'cycling':
        {
          const distance = inputDistance.value;
          const duration = inputDuration.value;
          const coords = this.#lastCoords;
          const elevation = inputElevation.value;

          if (!verifyInputs(distance, duration, elevation)) {
            alert('Please Enter correct values!!!');
            return;
          }

          workout = new Cycling(
            distance,
            duration,
            coords,
            Date.now(),
            elevation
          );
        }
        break;
      default:
        break;
    }
    this.#workouts.push(workout);
    this._setLocalStorage();
    this._renderWorkout(workout);
    this._renderWorkoutMarker(workout);
    this._hideform();
    console.log(this.#workouts);
  }

  _renderUI(workout) {
    this._renderWorkout(workout);
    this._renderWorkoutMarker(workout);
  }

  _renderWorkout(workout) {
    let html = `
      <li class="workout workout--${workout.type.toLowerCase()}" data-id="${
      workout.id
    }">
        <h2 class="workout__title">${workout.description}</h2>
        <div class="workout__edit">
        <span class="material-icons md-18" id="edit">
        edit
        </span>
        <span class="material-icons md-18" id="delete">
        delete
        </span></div>
        <div class="workout__details">
          <span class="workout__icon">${
            workout.type == 'Running' ? '🏃‍♂️' : '🚴‍♀️'
          }</span>
          <span class="workout__value">${workout.distance}</span>
          <span class="workout__unit">km</span>
        </div>
        <div class="workout__details">
          <span class="workout__icon">⏱</span>
          <span class="workout__value">${workout.duration}</span>
          <span class="workout__unit">min</span>
        </div>
    `;

    if (workout.type == 'Running')
      html += `
        <div class="workout__details">
          <span class="workout__icon">⚡️</span>
          <span class="workout__value">${workout.pace.toFixed(1)}</span>
          <span class="workout__unit">min/km</span>
        </div>
        <div class="workout__details">
          <span class="workout__icon">🦶🏼</span>
          <span class="workout__value">${workout.cadence}</span>
          <span class="workout__unit">spm</span>
        </div>
      </li>
      `;

    if (workout.type == 'Cycling')
      html += `
        <div class="workout__details">
          <span class="workout__icon">⚡️</span>
          <span class="workout__value">${workout.speed.toFixed(1)}</span>
          <span class="workout__unit">km/h</span>
        </div>
        <div class="workout__details">
          <span class="workout__icon">⛰</span>
          <span class="workout__value">${workout.elevationGain}</span>
          <span class="workout__unit">m</span>
        </div>
      </li>
      `;

    form.insertAdjacentHTML('afterend', html);
    const tag = document.querySelector(`li[data-id="${workout.id}"]`);
    const btnEdit = document.querySelector('#edit');
    const btnDelete = document.querySelector('#delete');

    const tagHandler = function (e) {
      e.preventDefault();

      //Prevent movement when delete clicked
      //Remove the tag when clicked on the edit and delete buttons
      if (e.target == btnDelete) {
        return;
      }
      this._moveToPopup(workout.coords);
    };
    tag.addEventListener('click', tagHandler.bind(this));

    btnDelete.addEventListener('click', e => {
      e.preventDefault();
      console.log(`Delete - ${workout.id} and Target ${e.target}`);
      tag.removeEventListener('click', tagHandler.bind(this));
      tag.parentNode.removeChild(tag);
      document;
      this._removeWorkOut(e, workout);
    });

    btnEdit.addEventListener('click', e => {
      e.preventDefault();
      console.log(`Edit - ${workout.id} and Target ${e.target}`);
      tag.style.display = 'none';
      this._editForm(workout, tag);
    });
  }

  _moveToPopup(coords) {
    this.#map.setView(coords, this.mapZoomLevel);
  }

  _renderWorkoutMarker(workout) {
    workout.marker = L.marker(workout.coords, { riseOnHover: true }).addTo(
      this.#map
    );

    workout.marker
      .bindPopup(
        L.popup({
          minwidth: 100,
          maxwidth: 250,
          autoClose: false,
          closeOnClick: false,
          className:
            workout instanceof Running || workout.type == 'Running'
              ? 'running-popup'
              : 'cycling-popup',
        }).setContent(workout.description)
      )
      .openPopup();
  }

  _hideform() {
    form.reset();
    form.classList.add('hidden');
  }

  _removeWorkOut(e, workout) {
    this.#workouts = this.#workouts.filter(item => {
      console.log(`workout.id : ${workout.id}`, `item.id : ${item.id}`);
      return item.id != workout.id;
    });

    this.#map.removeLayer(workout.marker);
    this._setLocalStorage();
  }

  resetLocalStorage() {
    localStorage.removeItem('myWorkouts');
    location.reload();
  }
}

const obj = new App();
